function Person(firstName, lastName){
	this.firstName = firstName;
	this.lastName = lastName;
	Person.prototype.sayName = function(){
	//display in the console
	//Hi My name is...........!
	console.log(`Hi My name is ${this.firstName} ${this.lastName}`);
	}
}

let personA = new Person('Alan', 'Beraquit')
personA.sayName()